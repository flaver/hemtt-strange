class CfgVehicles {
	class FDF_DUMMY_fat_m05_maasto_sin_2;
	class PVM_DUMMY_flaver: FDF_DUMMY_fat_m05_maasto_sin_2 {
		author = "Ianassa + flaver";
		_generalMacro = "PVM_DUMMY_flaver";
		uniformClass = "pvm_custom_m05_flaver";
		hiddenSelectionsTextures[] = {QPATHTOF(data\textures\uniform_flaver.paa)};
	};

	class PVM_DUMMY_lad: FDF_DUMMY_fat_m05_maasto_sin_2 {
		author = "Ianassa + flaver";
		_generalMacro = "PVM_DUMMY_lad";
		uniformClass = "pvm_custom_m05_lad";
		hiddenSelectionsTextures[] = {QPATHTOF(data\textures\uniform_lad.paa)};
	};

	class PVM_DUMMY_bane: FDF_DUMMY_fat_m05_maasto_sin_2 {
		author = "Ianassa + flaver";
		_generalMacro = "PVM_DUMMY_bane";
		uniformClass = "pvm_custom_m05_bane";
		hiddenSelectionsTextures[] = {QPATHTOF(data\textures\uniform_bane.paa)};
	};

	class PVM_DUMMY_easy: FDF_DUMMY_fat_m05_maasto_sin_2 {
		author = "Ianassa + flaver";
		_generalMacro = "PVM_DUMMY_easy";
		uniformClass = "pvm_custom_m05_easy";
		hiddenSelectionsTextures[] = {QPATHTOF(data\textures\uniform_easy.paa)};
	};

	class PVM_DUMMY_rudi: FDF_DUMMY_fat_m05_maasto_sin_2 {
		author = "Ianassa + flaver";
		_generalMacro = "PVM_DUMMY_rudi";
		uniformClass = "pvm_custom_m05_rudi";
		hiddenSelectionsTextures[] = {QPATHTOF(data\textures\uniform_rudi.paa)};
	};

	class PVM_DUMMY_warborn: FDF_DUMMY_fat_m05_maasto_sin_2 {
		author = "Ianassa + flaver";
		_generalMacro = "PVM_DUMMY_warborn";
		uniformClass = "pvm_custom_m05_warborn";
		hiddenSelectionsTextures[] = {QPATHTOF(data\textures\uniform_warborn.paa)};
	};
};