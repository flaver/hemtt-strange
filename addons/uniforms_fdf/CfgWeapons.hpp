class CfgWeapons {
  // Externals
  class FDF_fat_m04_helle_sin_2 {
    class ItemInfo;
  };

  class FDF_fat_rs_m04_helle_tac_1 {
    class ItemInfo;
  };

  class FDF_fat_m05_pakkas_tac_2 {
    class ItemInfo;
  };

  class FDF_fat_m05_lumi_sin_1 {
    class ItemInfo;
  };

  class FDF_fat_m05_maasto_sin_2 {
    class ItemInfo;
  };

  class FDF_fat_rs_m05_maasto_sin_2 {
    class ItemInfo;
  };

  class FDF_sf_fat_maasto_tac_1 {
    class ItemInfo;
  };

  class pvm_fdf_d :FDF_fat_m04_helle_sin_2 {
    // VT-Mod comp
    class ItemInfo: ItemInfo {
      containerClass="Supply0";
    };
    // Change display name etc, just to find it better
    displayName = "[PB] FDF M04 Arid";
    scope=2;
    author="Ianassa + [Pori Brig.] flaver";
  };

  class pvm_fdf_rs_d :FDF_fat_rs_m04_helle_tac_1 {
    // VT-Mod comp
    class ItemInfo: ItemInfo {
      containerClass="Supply0";
    };
    // Change display name etc, just to find it better
    displayName = "[PB] FDF M04 Arid (Rolled Up)";
    scope=2;
    author="Ianassa + [Pori Brig.] flaver";
  };

  class pvm_fdf_pakkas :FDF_fat_m05_pakkas_tac_2 {
    // VT-Mod comp
    class ItemInfo: ItemInfo {
      containerClass="Supply0";
    };
    // Change display name etc, just to find it better
    displayName = "[PB] FDF M05 Cold Weather";
    scope=2;
    author="Ianassa + [Pori Brig.] flaver";
  };

  class pvm_fdf_lumi :FDF_fat_m05_lumi_sin_1 {
    // VT-Mod comp
    class ItemInfo: ItemInfo {
      containerClass="Supply0";
    };
    // Change display name etc, just to find it better
    displayName = "[PB] FDF M05 Snow";
    scope=2;
    author="Ianassa + [Pori Brig.] flaver";
  };

  class pvm_fdf_maasto :FDF_fat_m05_maasto_sin_2 {
    // VT-Mod comp
    class ItemInfo: ItemInfo {
      containerClass="Supply0";
    };
      // Change display name etc, just to find it better
    displayName = "[PB] FDF M05 New";
    scope=2;
    author="Ianassa + [Pori Brig.] flaver";
  };

  class pvm_fdf_rs_maasto :FDF_fat_rs_m05_maasto_sin_2 {
    // VT-Mod comp
    class ItemInfo: ItemInfo {
      containerClass="Supply0";
    };
    // Change display name etc, just to find it better
    displayName = "[PB] FDF M05 New (Rolled Up)";
    scope=2;
    author="Ianassa + [Pori Brig.] flaver";
  };


  // Start custom unifroms
  class pvm_custom_m05_base: FDF_fat_m05_maasto_sin_2 {
    scope=1;
    author = "Ianassa + flaver";
  };

  class pvm_custom_m05_flaver: pvm_custom_m05_base {
    displayName = "[PB] FDF M05 (flaver)";
    scope=2;
    hiddenSelectionsTextures[] = {QPATHTOF(data\textures\uniform_flaver.paa)};

    class ItemInfo: ItemInfo {
      uniformClass = "PVM_DUMMY_flaver";
    };
  };

  class pvm_custom_m05_lad: pvm_custom_m05_base {
    displayName = "[PB] FDF M05 (lad)";
    scope=2;
    hiddenSelectionsTextures[] = {QPATHTOF(data\textures\uniform_lad.paa)};

    class ItemInfo: ItemInfo {
      uniformClass = "PVM_DUMMY_lad";
    };
  };

  class pvm_custom_m05_bane: pvm_custom_m05_base {
    displayName = "[PB] FDF M05 (bane)";
    scope=2;
    hiddenSelectionsTextures[] = {QPATHTOF(data\textures\uniform_bane.paa)};

    class ItemInfo: ItemInfo {
      uniformClass = "PVM_DUMMY_bane";
    };
  };

  class pvm_custom_m05_easy: pvm_custom_m05_base {
    displayName = "[PB] FDF M05 (easy)";
    scope=2;
    hiddenSelectionsTextures[] = {QPATHTOF(data\textures\uniform_easy.paa)};

    class ItemInfo: ItemInfo {
      uniformClass = "PVM_DUMMY_easy";
    };
  };

  class pvm_custom_m05_rudi: pvm_custom_m05_base {
    displayName = "[PB] FDF M05 (rudi)";
    scope=2;
    hiddenSelectionsTextures[] = {QPATHTOF(data\textures\uniform_rudi.paa)};

    class ItemInfo: ItemInfo {
      uniformClass = "PVM_DUMMY_rudi";
    };
  };

  class pvm_custom_m05_warborn: pvm_custom_m05_base {
    displayName = "[PB] FDF M05 (warborn)";
    scope=2;
    hiddenSelectionsTextures[] = {QPATHTOF(data\textures\uniform_warborn.paa)};

    class ItemInfo: ItemInfo {
      uniformClass = "PVM_DUMMY_warborn";
    };
  };
};
