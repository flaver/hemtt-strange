#include "script_component.hpp"
class CfgPatches {
  class ADDON {
    name = COMPONENT;
    units[] = {"PVM_DUMMY_flaver", "PVM_DUMMY_lad", "PVM_DUMMY_bane", "PVM_DUMMY_easy", "PVM_DUMMY_rudi", "PVM_DUMMY_warborn"};
    weapons[] = {"pvm_custom_m05_flaver", "pvm_custom_m05_lad", "pvm_custom_m05_bane", "pvm_custom_m05_easy", "pvm_custom_m05_rudi", "pvm_custom_m05_warborn"};
    requiredVersion = REQUIRED_VERSION;
    requiredAddons[] = {};
    author = "flaver,Realeasy";
    VERSION_CONFIG;
  };
};


#include "CfgEventHandlers.hpp"
#include "CfgVehicles.hpp"
#include "CfgWeapons.hpp"
