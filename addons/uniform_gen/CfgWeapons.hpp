class CfgWeapons {

  // Externals
  class jack_gnd_pullover_uniform_1 {
    class ItemInfo;
  };
  class jack_gnd_pullover_uniform_2 {
    class ItemInfo;
  };
  class jack_gnd_pullover_uniform_3 {
    class ItemInfo;
  };
  class jack_gnd_pullover_uniform_4 {
    class ItemInfo;
  };
  class jack_gnd_pullover_uniform_5 {
    class ItemInfo;
  };
  class jack_gnd_pullover_uniform_6 {
    class ItemInfo;
  };
  class jack_gnd_pullover_uniform_7 {
    class ItemInfo;
  };
  class jack_4s_uniform_1 {
    class ItemInfo;
  };
  class jack_4s_uniform_2 {
    class ItemInfo;
  };
  class jack_4s_uniform_6 {
    class ItemInfo;
  };
  class jack_4s_uniform_7 {
    class ItemInfo;
  };

  class pvm_gen_recrut: jack_gnd_pullover_uniform_1 {
    class ItemInfo: ItemInfo {
      containerClass="Supply0";
    };
    // Change display name etc, just to find it better
    displayName = "[PB] GEN Recrut";
    scope=2;
    author="Jack + [Pori Brig.] Bane";
  };

  class pvm_gen_brigadier: jack_gnd_pullover_uniform_2 {
    class ItemInfo: ItemInfo {
      containerClass="Supply0";
    };
    // Change display name etc, just to find it better
    displayName = "[PB] GEN Brigadier";
    scope=2;
    author="Jack + [Pori Brig.] Bane";
  };

  class pvm_gen_brigadier_chef: jack_gnd_pullover_uniform_3 {
    class ItemInfo: ItemInfo {
      containerClass="Supply0";
    };
    // Change display name etc, just to find it better
    displayName = "[PB] GEN Brigadier-Chef";
    scope=2;
    author="Jack + [Pori Brig.] Bane";
  };

  class pvm_gen_marechal: jack_gnd_pullover_uniform_4 {
    class ItemInfo: ItemInfo {
      containerClass="Supply0";
    };
    // Change display name etc, just to find it better
    displayName = "[PB] GEN Maréchal-des-Logis";
    scope=2;
    author="Jack + [Pori Brig.] Bane";
  };

  class pvm_gen_gendarm: jack_gnd_pullover_uniform_5 {
    class ItemInfo: ItemInfo {
      containerClass="Supply0";
    };
    // Change display name etc, just to find it better
    displayName = "[PB] GEN Gendarm";
    scope=2;
    author="Jack + [Pori Brig.] Bane";
  };

  class pvm_gen_marechal_chef: jack_gnd_pullover_uniform_6 {
    class ItemInfo: ItemInfo {
      containerClass="Supply0";
    };
    // Change display name etc, just to find it better
    displayName = "[PB] GEN Maréchal-des-Logis-Chef";
    scope=2;
    author="Jack + [Pori Brig.] Bane";
  };

  class pvm_gen_adjudant: jack_gnd_pullover_uniform_7 {
    class ItemInfo: ItemInfo {
      containerClass="Supply0";
    };
    // Change display name etc, just to find it better
    displayName = "[PB] GEN Adjudant";
    scope=2;
    author="Jack + [Pori Brig.] Bane";
  };

  class pvm_gen_gign_camo: jack_4s_uniform_1 {
    class ItemInfo: ItemInfo {
      containerClass="Supply0";
    };
    // Change display name etc, just to find it better
    displayName = "[PB] GEN GIGN (Camo)";
    scope=2;
    author="Jack + [Pori Brig.] Bane";
  };

  class pvm_gen_gign_blk: jack_4s_uniform_2 {
    class ItemInfo: ItemInfo {
      containerClass="Supply0";
    };
    // Change display name etc, just to find it better
    displayName = "[PB] GEN GIGN (Black)";
    scope=2;
    author="Jack + [Pori Brig.] Bane";
  };

  class pvm_gen_gign_mc_camo: jack_4s_uniform_6 {
    class ItemInfo: ItemInfo {
      containerClass="Supply0";
    };
    // Change display name etc, just to find it better
    displayName = "[PB] GEN GIGN MC (Camo)";
    scope=2;
    author="Jack + [Pori Brig.] Bane";
  };

  class pvm_gen_gign_mc_blk: jack_4s_uniform_7 {
    class ItemInfo: ItemInfo {
      containerClass="Supply0";
    };
    // Change display name etc, just to find it better
    displayName = "[PB] GEN GIGN MC (Black)";
    scope=2;
    author="Jack + [Pori Brig.] Bane";
  };
};
