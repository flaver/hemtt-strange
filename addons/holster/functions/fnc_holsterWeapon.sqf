#include "script_component.hpp"
/*
 * Author: flaver
 * This will holster the weapon of the player
 * In the pistol holster or on the back
 *
 * Arguments:
 * 0: _player <OBJECT>
 *
 * Return Value:
 * void
 *
 * Example:
 * [player] call pvm_holster_fnc_holsterWeapon
 *
 */

params["_player"];

_player action ["SWITCHWEAPON",_player,_player,-1];
waitUntil {currentWeapon _player == "" or {primaryWeapon _player == "" && handgunWeapon _player == ""}};
