// Register keybinding
#include "script_component.hpp"
#include "\a3\editor_f\Data\Scripts\dikCodes.h"

if (!hasInterface) exitWith {};

[
  "Extended Veteran Mod",
  QGVAR(HolsterWeapon),
  "Holster Weapon",
  {
    [player] call FUNC(holsterWeapon);
  },
  "",
  [DIK_0, [false, false, false]]
] call CBA_fnc_addKeybind;
