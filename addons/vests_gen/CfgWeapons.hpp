class CfgWeapons {
  class jack_ceinturon_gend_1 {
    class ItemInfo;
  };
  class jack_gilet_lourd_gend_2 {
    class ItemInfo;
  };
  class jack_gilet_leger_gend_1 {
    class ItemInfo;
  };
  class jack_gilet_ultra_lourd_gend_3 {
    class ItemInfo;
  };

//VEST FOR Gendamerie, Rangemaster
  class pvm_gen_rangemaster: jack_ceinturon_gend_1 {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {1, 45};
      handgunmagazines[] = {4, 20};
      uglmagazines = 0;
      grenades = 0;
    };
    class ItemInfo: ItemInfo {
      armor = 30;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] Gen Rangemasterbelt";
    scope=2;
    author="Jack + [Pori Brig.] Bane";
  };

  //VEST FOR GIGN light
  class pvm_gen_light_gign: jack_gilet_lourd_gend_2 {
    class VTN_VESTINFO {
      primarymagazines[] = {8, 45};
      handgunmagazines[] = {2, 20};
      uglmagazines = 0;
      grenades = 4;
    };
    class ItemInfo :ItemInfo {
      armor = 30;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] Gen Light Vest GIGN";
    scope=2;
    author="Jack + [Pori Brig.] Bane";
  };

  // VEST FOR Gendamerie, Tactical Vest
  class pvm_gen_tactical_vest :jack_gilet_leger_gend_1 {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {4, 45};
      handgunmagazines[] = {2, 20};
      uglmagazines = 0;
      grenades = 0;
    };
    class ItemInfo: ItemInfo {
      armor = 20;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] Gen Tactical Vest";
    scope=2;
    author="Jack + [Pori Brig.] Bane";
  };

//VEST FOR GIGN Ultra Heavy
class pvm_gen_ultra_gign: jack_gilet_ultra_lourd_gend_3 {
  class VTN_VESTINFO {
    primarymagazines[] = {8, 45};
    handgunmagazines[] = {2, 20};
    uglmagazines = 0;
    grenades = 4;
  };
  class ItemInfo :ItemInfo {
    armor = 40;
  };

  // Change display name etc, just to find it better
  displayName = "[PB] Gen Ultra Heavy Vest GIGN";
  scope=2;
  author="Jack + [Pori Brig.] Bane";
  };
};
