class CfgWeapons {
  class rhsusf_iotv_ocp_Grenadier;
  class rhsusf_iotv_ocp_Medic;
  class rhsusf_iotv_ocp;
  class rhsusf_iotv_ocp_Repair;
  class rhsusf_iotv_ocp_Rifleman;
  class rhsusf_iotv_ocp_SAW;
  class rhsusf_iotv_ocp_Squadleader;
  class rhsusf_iotv_ocp_Teamleader;

  class rhsusf_spc_corpsman;
  class rhsusf_spc_crewman;
  class rhsusf_spc_mg;
  class rhsusf_spc_marksman;
  class rhsusf_spc_rifleman;
  class rhsusf_spc_sniper;
  class rhsusf_spc_squadleader;
  class rhsusf_spc_teamleader;

  class PVM_iotv_ocp_Grenadier: rhsusf_iotv_ocp_Grenadier{
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {7, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 6;
      grenades = 2;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] US Army Grenadier";
    scope=2;
    author="RHS Team + [Pori Brig.] flaver";
  };

  class PVM_iotv_ocp_Medic: rhsusf_iotv_ocp_Medic{
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {6, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] US Army Medic";
    scope=2;
    author="RHS Team + [Pori Brig.] flaver";
  };

  class PVM_iotv_ocp_Medic: rhsusf_iotv_ocp_Medic{
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {6, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] US Army Medic";
    scope=2;
    author="RHS Team + [Pori Brig.] flaver";
  };

  class PVM_iotv_ocp: rhsusf_iotv_ocp{
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {0, 0};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 0;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] US Army";
    scope=2;
    author="RHS Team + [Pori Brig.] flaver";
  };

  class PVM_iotv_ocp_Repair: rhsusf_iotv_ocp_Repair{
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {3, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 2;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] US Army Repair";
    scope=2;
    author="RHS Team + [Pori Brig.] flaver";
  };

  class PVM_iotv_ocp_Rifleman: rhsusf_iotv_ocp_Rifleman{
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {6, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 4;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] US Army Rifleman";
    scope=2;
    author="RHS Team + [Pori Brig.] flaver";
  };

  class PVM_iotv_ocp_SAW: rhsusf_iotv_ocp_SAW{
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {4, 270};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 2;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] US Army SMAW";
    scope=2;
    author="RHS Team + [Pori Brig.] flaver";
  };

  class PVM_iotv_ocp_Squadleader: rhsusf_iotv_ocp_Squadleader{
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {6, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 4;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] US Army Squadleader";
    scope=2;
    author="RHS Team + [Pori Brig.] flaver";
  };

  class PVM_iotv_ocp_Squadleader: rhsusf_iotv_ocp_Squadleader{
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {6, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 4;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] US Army Squadleader";
    scope=2;
    author="RHS Team + [Pori Brig.] flaver";
  };

  class PVM_iotv_ocp_Teamleader: rhsusf_iotv_ocp_Teamleader{
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {6, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 2;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] US Army Teamleader";
    scope=2;
    author="RHS Team + [Pori Brig.] flaver";
  };

  class PVM_spc_corpsman: rhsusf_spc_corpsman{
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {6, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 3;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] USMC Corpsman";
    scope=2;
    author="RHS Team + [Pori Brig.] flaver";
  };

  class PVM_spc_crewman: rhsusf_spc_crewman {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {2, 45};
      handgunmagazines[] = {2, 20};
      uglmagazines = 0;
      grenades = 0;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] USMC Crewman";
    scope=2;
    author="RHS Team + [Pori Brig.] flaver";
  };

  class PVM_spc_mg: rhsusf_spc_mg {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {3, 270};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 2;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] USMC MG";
    scope=2;
    author="RHS Team + [Pori Brig.] flaver";
  };

  class PVM_spc_marksman: rhsusf_spc_marksman {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {3, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 0;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] USMC Marksman";
    scope=2;
    author="RHS Team + [Pori Brig.] flaver";
  };

  class PVM_spc_rifleman: rhsusf_spc_rifleman {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {10, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 2;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] USMC Rifleman";
    scope=2;
    author="RHS Team + [Pori Brig.] flaver";
  };

  class PVM_spc_sniper: rhsusf_spc_sniper {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {2, 45};
      handgunmagazines[] = {2, 20};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] USMC Scout Sniper";
    scope=2;
    author="RHS Team + [Pori Brig.] flaver";
  };

  class PVM_spc_squadleader: rhsusf_spc_squadleader {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {5, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 2;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] USMC Squadleader";
    scope=2;
    author="RHS Team + [Pori Brig.] flaver";
  };

  class PVM_spc_teamleader: rhsusf_spc_teamleader {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {10, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 6;
      grenades = 2;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] USMC Teamleader";
    scope=2;
    author="RHS Team + [Pori Brig.] flaver";
  };
};
