class CfgWeapons {
  class FDF_VEST_21 {
    class ItemInfo;
  };

  class FDF_VEST_22 {
    class ItemInfo;
  };

  class ffp_m05combatvest {
    class ItemInfo;
  };
  class ffp_m05combatvest_grenade {
    class ItemInfo;
  };
  class ffp_m05combatvest_radio {
    class ItemInfo;
  };

  class milgp_v_marciras_assaulter_khk;
  class milgp_v_marciras_assaulter_belt_khk;
  class milgp_v_marciras_assaulter_rgr;
  class milgp_v_marciras_assaulter_belt_rgr;
  class milgp_v_marciras_grenadier_khk;
  class milgp_v_marciras_grenadier_belt_khk;
  class milgp_v_marciras_grenadier_rgr;
  class milgp_v_marciras_grenadier_belt_rgr;
  class milgp_v_marciras_hgunner_khk;
  class milgp_v_marciras_hgunner_belt_khk;
  class milgp_v_marciras_hgunner_rgr;
  class milgp_v_marciras_hgunner_belt_rgr;
  class milgp_v_marciras_marksman_khk;
  class milgp_v_marciras_marksman_belt_khk;
  class milgp_v_marciras_marksman_rgr;
  class milgp_v_marciras_marksman_belt_rgr;
  class milgp_v_marciras_medic_khk;
  class milgp_v_marciras_medic_belt_khk;
  class milgp_v_marciras_medic_rgr;
  class milgp_v_marciras_medic_belt_rgr;
  class milgp_v_marciras_teamleader_KHK;
  class milgp_v_marciras_teamleader_belt_KHK;
  class milgp_v_marciras_teamleader_RGR;
  class milgp_v_marciras_teamleader_belt_RGR;

  class pvm_fdf_d_light: FDF_VEST_21 {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {8, 45};
      handgunmagazines[] = {2, 20};
      uglmagazines = 0;
      grenades = 4;
    };
    class ItemInfo: ItemInfo {
      armor = 30;
    };

    // Change display name etc, just to find it better
    displayName = "PB FDF Light Vest (Sand)";
    scope=2;
    author="Ianassa + [Pori Brig.] flaver";
  };

  //VEST FOR MG
  class pvm_fdf_d_heavy: FDF_VEST_22 {
    class VTN_VESTINFO {
      primarymagazines[] = {2, 270};
      handgunmagazines[] = {2, 20};
      uglmagazines = 0;
      grenades = 4;
    };
    class ItemInfo :ItemInfo {
      armor = 30;
    };

    // Change display name etc, just to find it better
    displayName = "PB FDF Heavy Vest (Sand)";
    scope=2;
    author="Ianassa + [Pori Brig.] flaver";
  };

  // VEST FOR GL
  class pvm_fdf_d_light_gl :FDF_VEST_21 {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {8, 45};
      handgunmagazines[] = {2, 20};
      uglmagazines = 10;
      grenades = 0;
    };
    class ItemInfo: ItemInfo {
      armor = 30;
    };

    // Change display name etc, just to find it better
    displayName = "PB FDF Light Vest GL (Sand)";
    scope=2;
    author="Ianassa + [Pori Brig.] flaver";
  };

  // Vests for FRDF
  class pvm_milgp_v_marciras_assaulter_khk: milgp_v_marciras_assaulter_khk {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {6, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "PB FRDF Rifleman (M04)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };

  class pvm_milgp_v_marciras_assaulter_belt_khk: milgp_v_marciras_assaulter_belt_khk {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {10, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "PB FRDF Rifleman + Belt (M04)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };

  class pvm_milgp_v_marciras_assaulter_rgr: milgp_v_marciras_assaulter_rgr {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {6, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "PB FRDF Rifleman (M05)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver"
  };

  class pvm_milgp_v_marciras_assaulter_belt_rgr: milgp_v_marciras_assaulter_belt_rgr {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {10, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "PB FRDF Rifleman + Belt (M05)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };

  class pvm_milgp_v_marciras_grenadier_khk: milgp_v_marciras_grenadier_khk {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {8, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 6;
      grenades = 2;
    };

    // Change display name etc, just to find it better
    displayName = "PB FRDF Grenadier(M04)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };

  class pvm_milgp_v_marciras_grenadier_khk: milgp_v_marciras_grenadier_khk {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {8, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 6;
      grenades = 2;
    };

    // Change display name etc, just to find it better
    displayName = "PB FRDF Grenadier(M04)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };

  class pvm_milgp_v_marciras_grenadier_belt_khk: milgp_v_marciras_grenadier_belt_khk {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {12, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 12;
      grenades = 2;
    };

    // Change display name etc, just to find it better
    displayName = "PB FRDF Grenadier + Belt (M04)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };

  class pvm_milgp_v_marciras_grenadier_rgr: milgp_v_marciras_grenadier_rgr {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {8, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 6;
      grenades = 2;
    };

    // Change display name etc, just to find it better
    displayName = "PB FRDF Grenadier(M05)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };

  class pvm_milgp_v_marciras_grenadier_belt_rgr: milgp_v_marciras_grenadier_belt_rgr {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {12, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 12;
      grenades = 2;
    };

    // Change display name etc, just to find it better
    displayName = "PB FRDF Grenadier + Belt (M04)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };

  class pvm_milgp_v_marciras_hgunner_khk: milgp_v_marciras_hgunner_khk {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {3, 270};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "PB FRDF Machinegunner(M04)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };

  class pvm_milgp_v_marciras_hgunner_belt_khk: milgp_v_marciras_hgunner_belt_khk {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {5, 270};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "PB FRDF Machinegunner + Belt (M04)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };

  class pvm_milgp_v_marciras_hgunner_rgr: milgp_v_marciras_hgunner_rgr {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {3, 270};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "PB FRDF Machinegunner(M05)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };

  class pvm_milgp_v_marciras_hgunner_belt_rgr: milgp_v_marciras_hgunner_belt_rgr {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {5, 270};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "PB FRDF Machinegunner + Belt (M05)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };

  class pvm_milgp_v_marciras_marksman_khk: milgp_v_marciras_marksman_khk {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {6, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "PB FRDF Marksman (M04)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };

  class pvm_milgp_v_marciras_marksman_belt_khk: milgp_v_marciras_marksman_belt_khk {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {12, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "PB FRDF Marksman + Belt(M04)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };

  class pvm_milgp_v_marciras_marksman_rgr: milgp_v_marciras_marksman_rgr {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {6, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "PB FRDF Marksman (M05)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };

  class pvm_milgp_v_marciras_marksman_belt_rgr: milgp_v_marciras_marksman_belt_rgr {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {12, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "PB FRDF Marksman + Belt (M05)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };

  class pvm_milgp_v_marciras_medic_khk: milgp_v_marciras_medic_khk {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {4, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 2;
    };

    // Change display name etc, just to find it better
    displayName = "PB FRDF Medic (M04)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };

  class pvm_milgp_v_marciras_medic_belt_khk: milgp_v_marciras_medic_belt_khk {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {6, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 2;
    };

    // Change display name etc, just to find it better
    displayName = "PB FRDF Medic + Belt (M04)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };

  class pvm_milgp_v_marciras_medic_rgr: milgp_v_marciras_medic_rgr {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {4, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 2;
    };

    // Change display name etc, just to find it better
    displayName = "PB FRDF Medic (M05)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };

  class pvm_milgp_v_marciras_medic_belt_rgr: milgp_v_marciras_medic_belt_rgr {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {6, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 2;
    };

    // Change display name etc, just to find it better
    displayName = "PB FRDF Medic + Belt (M05)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };

  class pvm_milgp_v_marciras_teamleader_KHK: milgp_v_marciras_teamleader_KHK {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {4, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 2;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "PB FRDF Squadleader (M04)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };

  class pvm_milgp_v_marciras_teamleader_belt_KHK: milgp_v_marciras_teamleader_belt_KHK {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {6, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 6;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "PB FRDF Squadleader + Belt (M04)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };

  class pvm_milgp_v_marciras_teamleader_RGR: milgp_v_marciras_teamleader_RGR {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {4, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 2;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "PB FRDF Squadleader (M05)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };

  class pvm_milgp_v_marciras_teamleader_belt_RGR: milgp_v_marciras_teamleader_belt_RGR {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {6, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 6;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "PB FRDF Squadleader + Belt (M05)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };

  class pvm_fdf_vest_normal: ffp_m05combatvest {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {12, 45};
      handgunmagazines[] = {0, 20};
      uglmagazines = 0;
      grenades = 1;
    };

    class ItemInfo: ItemInfo {
      armor = 30;
    };

    // Change display name etc, just to find it better
    displayName = "PB FDF Vest Normal";
    scope=2;
    author="SFP + [Pori Brig.] flaver";
  };

  class pvm_fdf_vest_normal_mg: ffp_m05combatvest_grenade {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {3, 270};
      handgunmagazines[] = {0, 20};
      uglmagazines = 0;
      grenades = 1;
    };

    class ItemInfo: ItemInfo {
      armor = 30;
    };

    // Change display name etc, just to find it better
    displayName = "PB FDF Vest Normal MG";
    scope=2;
    author="SFP + [Pori Brig.] flaver";
  };

  class pvm_fdf_vest_normal_tl: ffp_m05combatvest_radio {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {12, 45};
      handgunmagazines[] = {0, 20};
      uglmagazines = 0;
      grenades = 1;
    };

    class ItemInfo: ItemInfo {
      armor = 30;
    };

    // Change display name etc, just to find it better
    displayName = "PB FDF Vest Normal TL";
    scope=2;
    author="SFP + [Pori Brig.] flaver";
  };
};
