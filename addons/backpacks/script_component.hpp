#define COMPONENT backpacks
#include "\z\pvm\addons\main\script_mod.hpp"

// #define DEBUG_MODE_FULL
// #define DISABLE_COMPILE_CACHE

#ifdef DEBUG_ENABLED_BACKPACKS
    #define DEBUG_MODE_FULL
#endif
    #ifdef DEBUG_SETTINGS_BACKPACKS
    #define DEBUG_SETTINGS DEBUG_SETTINGS_BACKPACKS
#endif

#include "\z\pvm\addons\main\script_macros.hpp"
